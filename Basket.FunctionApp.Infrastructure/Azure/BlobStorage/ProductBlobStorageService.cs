﻿using System.Text;
using System.Text.Json;
using Azure.Storage.Blobs;
using Basket.FunctionApp.Domain.Azure.BlobStorage;
using Basket.FunctionApp.Shared.Models.Requests;
using FluentResults;

namespace Basket.FunctionApp.Infrastructure.Azure.BlobStorage
{
    public class ProductBlobStorageService : IProductBlobStorageService
    {
        private const string ContainerName = "product-container";
        private readonly BlobServiceClient _blobServiceClient;

        public ProductBlobStorageService(BlobServiceClient blobServiceClient)
        {
            _blobServiceClient = blobServiceClient;
        }

        public async Task<Result> UploadAsync(ProductRequest[] productRequests)
        {
            string json = JsonSerializer.Serialize(productRequests);

            BlobContainerClient client = _blobServiceClient.GetBlobContainerClient(ContainerName);

            string blobName = $"product_{Guid.NewGuid()}.json";

            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(json)))
            {
                await client.UploadBlobAsync(blobName, stream);
            }

            return Result.Ok();
        }
    }
}
