﻿namespace Basket.FunctionApp.Shared.Models.Requests
{
    public class ProductRequest
    {
        public ProductRequest(int id, string name, int quantity, decimal price)
        {
            Id = id;
            Name = name;
            Quantity = quantity;
            Price = price;
        }

        public int Id { get; }

        public string Name { get; }

        public int Quantity { get; }

        public decimal Price { get; }
    }
}
