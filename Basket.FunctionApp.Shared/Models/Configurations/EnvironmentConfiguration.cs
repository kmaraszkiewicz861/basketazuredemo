﻿namespace Basket.FunctionApp.Shared.Models.Configurations
{
    public class EnvironmentConfiguration
    {
        public static string BlobStorageConnectionString => Environment.GetEnvironmentVariable("BlobStorageConnectionString")!;
    }
}
