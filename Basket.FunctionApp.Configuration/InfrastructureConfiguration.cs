﻿using Azure.Storage.Blobs;
using Basket.FunctionApp.Domain.Azure.BlobStorage;
using Basket.FunctionApp.Infrastructure.Azure.BlobStorage;
using Basket.FunctionApp.Shared.Models.Configurations;
using Microsoft.Extensions.DependencyInjection;

namespace Basket.FunctionApp.Configuration
{
    public static class InfrastructureConfiguration
    {
        public static IServiceCollection RegisterInfrastructureComponents(this IServiceCollection services)
        {
            services.AddScoped(services => new BlobServiceClient(EnvironmentConfiguration.BlobStorageConnectionString));

            services.RegisterAzureSergices();

            return services;
        }

        private static IServiceCollection RegisterAzureSergices(this IServiceCollection services)
        {
            services.AddScoped<IProductBlobStorageService, ProductBlobStorageService>();

            return services;
        }
    }
}
