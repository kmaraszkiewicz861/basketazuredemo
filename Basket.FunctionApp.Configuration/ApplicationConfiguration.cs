﻿using Basket.FunctionApp.Application.Handlers.CommandHandlers;
using Basket.FunctionApp.Application.Validators;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;

namespace Basket.FunctionApp.Configuration
{
    public static class ApplicationConfiguration
    {
        public static IServiceCollection RegisterApplicationComponents(this IServiceCollection services)
        {
            services.AddMediatR(config =>
            {
                config.RegisterServicesFromAssembly(typeof(SendProductsDataCommandHandler).Assembly);
            });

            services.AddValidatorsFromAssembly(typeof(SendProductsDataCommandValidator).Assembly);

            return services;
        }
    }
}
