﻿using AutoFixture;

namespace Basket.FunctionApp.Application.Tests.Validators
{
    public abstract class TestBase
    {
        protected Fixture Fixture { get; }

        public TestBase()
        {
            Fixture = new Fixture();
        }
    }
}