using Basket.FunctionApp.Application.Handlers.Commands;
using Basket.FunctionApp.Application.Validators;
using Basket.FunctionApp.Shared.Models.Requests;
using FluentAssertions;
using FluentValidation.Results;

namespace Basket.FunctionApp.Application.Tests.Validators
{

    public class SendProductsDataCommandValidatorTest : TestBase
    {
        private readonly Random _random = new();

        [Fact]
        public void ShouldReturnValid_WhenModelHasAllRequiredData()
        {
            SendProductsDataCommandValidator validator = new();

            var model = GenerateSendProductsDataCommand();

            ValidationResult result = validator.Validate(model);

            result.IsValid.Should().BeTrue();
        }

        [Theory]
        [MemberData(nameof(NamePropertyTestInvalidData))]
        public void ShouldReturnInvalid_WhenModelHasEmptyOrNullableNameProperty(string name)
        {
            SendProductsDataCommandValidator validator = new();

            var model = GenerateSendProductsDataCommand(new ProductRequest(99, name, _random.Next(1, 99), _random.Next(1, 999)));

            ValidationResult result = validator.Validate(model);

            result.IsValid.Should().BeFalse();
        }

        public static IEnumerable<object[]> NamePropertyTestInvalidData()
        {
            yield return new string[] { string.Empty };
            yield return new string[] { "" };
            yield return new string[] { null! };
        }

        private SendProductsDataCommand GenerateSendProductsDataCommand(ProductRequest product = default!)
        {
            List<ProductRequest> modelChildren = new();

            for (int index = 1; index <= 3; index++)
            {
                modelChildren.Add(new ProductRequest(index, $"Name{Guid.NewGuid()}", _random.Next(1, 99), _random.Next(1, 999)));
            }

            if (product is not null)
            {
                modelChildren.Add(product);
            }

            return new(modelChildren.ToArray());
        }
    }
}