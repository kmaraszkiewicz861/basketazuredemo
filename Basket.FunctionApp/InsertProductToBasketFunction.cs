using System.Net;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Basket.FunctionApp.Application.Handlers.Commands;
using Basket.FunctionApp.Shared.Models.Requests;
using FluentResults;
using MediatR;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;

namespace Basket.FunctionApp
{
    public class InsertProductToBasketFunction
    {
        private readonly ILogger _logger;

        private readonly IMediator _mediator;

        public InsertProductToBasketFunction(ILoggerFactory loggerFactory, IMediator mediator)
        {
            _logger = loggerFactory.CreateLogger<InsertProductToBasketFunction>();
            _mediator = mediator;
        }

        [Function("Function1")]
        public async Task<HttpResponseData> Run([HttpTrigger(AuthorizationLevel.Function, "post")] HttpRequestData request)
        {
            _logger.LogInformation("C# HTTP trigger function processed a request.");

            ProductRequest[]? products = await JsonSerializer.DeserializeAsync<ProductRequest[]>(request.Body);
            HttpResponseData response = request.CreateResponse(HttpStatusCode.OK);

            if (products is null)
            {
                return await CreateResponseAsync(request, 
                    HttpStatusCode.BadRequest, 
                    new { ErrorMessage = "The model is not a valid product list" });
            }

            Result result = await _mediator.Send(new SendProductsDataCommand(products));

            response = request.CreateResponse(HttpStatusCode.OK);
            response.Headers.Add("Content-Type", "application/json; charset=utf-8");

            return await CreateResponseAsync(request,
                result.IsSuccess
                    ? HttpStatusCode.OK
                    : HttpStatusCode.BadRequest,
                new
                {
                    IsValid = result.IsSuccess,
                    ErrorMessage = result.Errors.Select(e => e.Message)
                });
        }

        private async Task<HttpResponseData> CreateResponseAsync(HttpRequestData request, HttpStatusCode statusCode, object message)
        {
            HttpResponseData response = request.CreateResponse(statusCode);

            response.Headers.Add("Content-Type", "application/json; charset=utf-8");

            await response.WriteAsJsonAsync(message);

            return response;
        }
    }
}
