﻿using FluentResults;
using FluentValidation;
using FluentValidation.Results;

namespace Basket.FunctionApp.Application.Extensions
{
    public static class ValidatorExtension
    {
        public async static Task<Result> ToResultAsync<TModel>(this IValidator<TModel> validator, TModel modelToValidate)
        {
            ValidationResult result = await validator.ValidateAsync(modelToValidate);

            if (!result.IsValid)
                return Result.Fail(result.Errors.Select(e => e.ErrorMessage));

            return Result.Ok();
        }
    }
}
