﻿using Basket.FunctionApp.Application.Handlers.Commands;
using FluentValidation;

namespace Basket.FunctionApp.Application.Validators
{
    public class SendProductsDataCommandValidator : AbstractValidator<SendProductsDataCommand>
    {
        public SendProductsDataCommandValidator()
        {
            RuleFor(x => x.ProductRequests).NotEmpty();

            RuleForEach(x => x.ProductRequests)
                .SetValidator(new ProductRequestValidator());
        }
    }
}
