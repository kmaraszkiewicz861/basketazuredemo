﻿using Basket.FunctionApp.Application.Extensions;
using Basket.FunctionApp.Application.Handlers.Commands;
using Basket.FunctionApp.Domain.Azure.BlobStorage;
using FluentResults;
using FluentValidation;
using MediatR;

namespace Basket.FunctionApp.Application.Handlers.CommandHandlers
{
    public class SendProductsDataCommandHandler : IRequestHandler<SendProductsDataCommand, Result>
    {
        private readonly IValidator<SendProductsDataCommand> _validator;

        private readonly IProductBlobStorageService _productBlobStorageService;

        public SendProductsDataCommandHandler(IValidator<SendProductsDataCommand> validator, IProductBlobStorageService productBlobStorageService)
        {
            _validator = validator;
            _productBlobStorageService = productBlobStorageService;
        }

        public async Task<Result> Handle(SendProductsDataCommand request, CancellationToken cancellationToken)
        {
            Result result = await _validator.ToResultAsync(request);

            if (!result.IsFailed)
                return result;

            return await _productBlobStorageService.UploadAsync(request.ProductRequests);
        }
    }
}
