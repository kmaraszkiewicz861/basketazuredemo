﻿using Basket.FunctionApp.Shared.Models.Requests;
using FluentResults;
using MediatR;

namespace Basket.FunctionApp.Application.Handlers.Commands
{
    public class SendProductsDataCommand : IRequest<Result>
    {
        public ProductRequest[] ProductRequests { get; }

        public SendProductsDataCommand(ProductRequest[] productRequests)
        {
            ProductRequests = productRequests;
        }
    }
}
