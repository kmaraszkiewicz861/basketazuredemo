﻿using Basket.FunctionApp.Shared.Models.Requests;
using FluentResults;

namespace Basket.FunctionApp.Domain.Azure.BlobStorage
{
    public interface IProductBlobStorageService
    {
        Task<Result> UploadAsync(ProductRequest[] productRequests);
    }
}
